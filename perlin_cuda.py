import math
import operator
import matplotlib.pyplot as plt
import numpy as np
from numba import cuda


def perlin_cuda(height, width, depth,
                octaves=1, persistence=0.5, lacunarity=2.0,
                repeatx=1024, repeaty=1024, repeatz=1024, base=0):
    rv = np.zeros((width, height, depth), dtype=np.float32)

    x, y, z = np.meshgrid(
        np.asarray([y * 5 / height for y in range(height)]),
        np.asarray([x * 5 / width for x in range(width)]),
        np.asarray([z * 0.5 / depth for z in range(depth)]))
    x = x.astype(np.float32)
    y = y.astype(np.float32)
    z = z.astype(np.float32)

    print(x.shape)
    print(y.shape)
    print(z.shape)

    vectors = np.array([
        [0, 1, 1, 1], [0, 1, 1, -1], [0, 1, -1, 1], [0, 1, -1, -1],
        [0, -1, 1, 1], [0, -1, 1, -1], [0, -1, -1, 1], [0, -1, -1, -1],
        [1, 0, 1, 1], [1, 0, 1, -1], [1, 0, -1, 1], [1, 0, -1, -1],
        [-1, 0, 1, 1], [-1, 0, 1, -1], [-1, 0, -1, 1], [-1, 0, -1, -1],
        [1, 1, 0, 1], [1, 1, 0, -1], [1, -1, 0, 1], [1, -1, 0, -1],
        [-1, 1, 0, 1], [-1, 1, 0, -1], [-1, -1, 0, 1], [-1, -1, 0, -1],
        [1, 1, 1, 0], [1, 1, -1, 0], [1, -1, 1, 0], [1, -1, -1, 0],
        [-1, 1, 1, 0], [-1, 1, -1, 0], [-1, -1, 1, 0], [-1, -1, -1, 0]], dtype=np.float32)

    threadsperblock = (16, 16)
    blockspergrid_x = math.ceil(rv.shape[0] / threadsperblock[0])
    blockspergrid_y = math.ceil(rv.shape[1] / threadsperblock[1])
    blockspergrid = (blockspergrid_x, blockspergrid_y)

    p = np.arange(256, dtype=int)
    np.random.shuffle(p)
    p = np.stack([p, p]).flatten()

    for d in range(depth):
        tmp = np.zeros((width, height), dtype=np.float32)
        _perlin_cuda[blockspergrid, threadsperblock](tmp,
                                                     x, y, z,
                                                     d, p, vectors,
                                                     repeatx, repeaty, repeatz,
                                                     base)
        rv[..., d] = tmp
        if octaves > 1:
            freq = 1.
            amp = 1.
            max = 0.
            for i in range(1, octaves):
                max += amp
                freq *= lacunarity
                amp *= persistence
                _perlin_cuda[blockspergrid, threadsperblock](tmp,
                                                             x * freq, y * freq, z * freq,
                                                             d, p, vectors,
                                                             int(repeatx * freq),
                                                             int(repeaty * freq),
                                                             int(repeatz * freq),
                                                             base)
                rv[..., d] += tmp



        rv[..., d] = tmp

    return rv


@cuda.jit(device=True)
def lerp(a, b, x):
    "linear interpolation"
    return a + x * (b - a)


@cuda.jit(device=True)
def fade(t):
    "6t^5 - 15t^4 + 10t^3"
    return 6 * t ** 5 - 15 * t ** 4 + 10 * t ** 3


@cuda.jit(device=True)
def gradient(h, x, y, z, vectors):
    "grad converts h to the right gradient vector and return the dot product with (x,y)"
    g = vectors[h % 15]

    return g[0] * x + g[1] * y + g[2] * z


@cuda.jit
def _perlin_cuda(rv, x_arr, y_arr, z_arr, d, p, vectors, repeatx, repeaty, repeatz, base):
    x_cord, y_cord = cuda.grid(2)
    if x_cord < rv.shape[0] and y_cord < rv.shape[1]:
        x = x_arr[x_cord, y_cord, d]
        y = y_arr[x_cord, y_cord, d]
        z = z_arr[x_cord, y_cord, d]

        i = int(math.fmod(x, repeatx))
        j = int(math.fmod(y, repeaty))
        k = int(math.fmod(z, repeatz))

        ii = int(math.fmod(i + 1, repeatx))
        jj = int(math.fmod(j + 1, repeaty))
        kk = int(math.fmod(k + 1, repeatz))

        i = operator.and_(i, 255) + base
        j = operator.and_(j, 255) + base
        k = operator.and_(k, 255) + base
        ii = operator.and_(ii, 255) + base
        jj = operator.and_(jj, 255) + base
        kk = operator.and_(kk, 255) + base

        # internal coordinates
        fx = x - i
        fy = y - j
        fz = z - k
        # fade factors
        u = fade(fx)
        v = fade(fy)
        w = fade(fz)

        A = p[i]
        AA = p[A + j]
        AB = p[A + jj]
        B = p[ii]
        BA = p[B + j]
        BB = p[B + jj]

        # noise components
        n000 = gradient(p[AA + k], fx, fy, fz, vectors)
        n001 = gradient(p[BA + k], fx - 1, fy, fz, vectors)
        n010 = gradient(p[AB + k], fx, fy - 1, fz, vectors)
        n011 = gradient(p[BB + k], fx - 1, fy - 1, fz, vectors)
        n100 = gradient(p[AA + kk], fx, fy, fz - 1, vectors)
        n101 = gradient(p[BA + kk], fx - 1, fy, fz - 1, vectors)
        n110 = gradient(p[AB + kk], fx, fy - 1, fz - 1, vectors)
        n111 = gradient(p[BB + kk], fx - 1, fy - 1, fz - 1, vectors)

        # combine noises
        x1 = lerp(n000, n001, u)
        x2 = lerp(n010, n011, u)
        x3 = lerp(n100, n101, u)
        x4 = lerp(n110, n111, u)

        x5 = lerp(x1, x2, v)
        x6 = lerp(x3, x4, v)
        rv[x_cord, y_cord] = lerp(x5, x6, w)


if __name__ == "__main__":
    noise_map = perlin_cuda(1000, 1000, 30, octaves=1)
    plt.imshow(np.transpose(noise_map[..., 0]))
    plt.show()
