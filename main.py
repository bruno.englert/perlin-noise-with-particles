import arcade
import numpy as np
import matplotlib.pyplot as plt

from perlin_c import perlin_c
from perlin_cuda import perlin_cuda
from perlin_numpy import perlin_numpy

SCREEN_WIDTH = 3000
SCREEN_HEIGHT = int(SCREEN_WIDTH / 16 * 9)
DEPTH = 50
SCREEN_TITLE = "Perline Noise with Particles"
noise_map = None
acceleration_field = np.zeros((SCREEN_WIDTH, SCREEN_HEIGHT, DEPTH, 2), dtype=np.float32)


class Particle:
    def __init__(self):
        self.prev_center_x = None
        self.prev_center_y = None
        self.center_x = None
        self.center_y = None

        self.delta_x = None
        self.delta_y = None

    def setup(self):
        self.center_x = SCREEN_WIDTH
        self.center_y = np.random.uniform(0, SCREEN_HEIGHT, 1)[0]
        self.prev_center_x = self.center_x
        self.prev_center_y = self.center_y
        self.delta_x = -10
        self.delta_y = 0

    def on_draw(self):
        arcade.draw_line(self.prev_center_x, self.prev_center_y,
                         self.center_x, self.center_y,
                         color=(0, 0, 0, 10), line_width=1)

    def on_update(self, delta_time, depth_idx):
        self.prev_center_x = self.center_x
        self.prev_center_y = self.center_y
        self.center_x += self.delta_x * delta_time
        self.center_y += self.delta_y * delta_time

        if 0 < self.center_x < SCREEN_WIDTH and 0 < self.center_y < SCREEN_HEIGHT:
            drag_x = self.delta_x ** 2 * -0.002 * np.sign(self.delta_x)
            drag_y = self.delta_y ** 2 * -0.002 * np.sign(self.delta_y)
            acceleration_pixel = acceleration_field[int(self.center_x)][int(int(self.center_y))][depth_idx]
            self.delta_x += (acceleration_pixel[0] + drag_x) * delta_time
            self.delta_y += (acceleration_pixel[1] + drag_y) * delta_time
        else:
            self.setup()


class MyGame(arcade.Window):
    """
    Main application class.
    """

    def __init__(self, width, height, title):
        """
        Initializer
        """
        # Call the parent class initializer
        super().__init__(width, height, title)
        # Set the background color
        arcade.set_background_color(arcade.color.WHITE)
        self.particles = [Particle() for _ in range(1000)]
        self.running = True
        self.depth_indices = list(range(DEPTH)) + list(reversed(range(DEPTH)))
        self.step = 0

    def setup(self):
        self.set_update_rate(1/300)
        arcade.start_render()
        [p.setup() for p in self.particles]

    def on_draw(self):
        [p.on_draw() for p in self.particles]

    def on_update(self, delta_time):
        if self.running:
            depth_idx = self.depth_indices[self.step]
            self.step += 1
            self.step %= (DEPTH*2)
            [p.on_update(delta_time, depth_idx) for p in self.particles]

    def on_key_press(self, key, modifiers):
        if key == arcade.key.SPACE:
            self.running = True


if __name__ == "__main__":
    noise_map = perlin_cuda(SCREEN_HEIGHT, SCREEN_WIDTH, DEPTH)
    plt.imshow(np.transpose(noise_map[..., 0]))
    plt.show()

    noise_map = (noise_map - noise_map.min()) / (noise_map.max() - noise_map.min())

    force_multiplier = 60
    acceleration_field[..., 0] = np.cos(noise_map * 2 * np.pi) * force_multiplier
    acceleration_field[..., 1] = np.sin(noise_map * 2 * np.pi) * force_multiplier

    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()
