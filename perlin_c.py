from noise import pnoise3
import numpy as np


def perlin_c(height, width, depth):
    rv = np.zeros((width, height, depth))
    for x in range(width):
        for y in range(height):
            for z in range(depth):
                rv[x, y, z] = pnoise3(x * 10 / width,
                                      y * 10 / height,
                                      z * 0.5 / depth)
    return rv
