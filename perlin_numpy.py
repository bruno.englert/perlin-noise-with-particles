import numpy as np


def perlin_numpy(height, width, depth,
                 octaves=1, persistence=0.5, lacunarity=2.0,
                 repeatx=1024, repeaty=1024, repeatz=1024, base=0):
    x, y, z = np.meshgrid(
        np.asarray([y * 5 / height for y in range(height)]),
        np.asarray([x * 5 / width for x in range(width)]),
        np.asarray([z * 0.5 / depth for z in range(depth)]))

    rv = _perlin(x, y, z, repeatx, repeaty, repeatz, base)
    if octaves > 1:
        freq = 1.
        amp = 1.
        max = 0.
        for i in range(1, octaves):
            max += amp
            freq *= lacunarity
            amp *= persistence
            rv += _perlin(x * freq, y * freq, z * freq,
                          int(repeatx * freq), int(repeaty * freq), int(repeatz * freq),
                          base)
        rv /= max
    return rv


def _perlin(x, y, z, repeatx, repeaty, repeatz, base, seed=0):
    # permutation table
    # np.random.seed(seed)
    p = np.arange(256, dtype=int)
    np.random.shuffle(p)
    p = np.stack([p, p]).flatten()
    # coordinates of the top-left
    i = np.remainder(x, repeatx).astype(int)
    j = np.remainder(y, repeaty).astype(int)
    k = np.remainder(z, repeatz).astype(int)

    ii = np.remainder(i + 1, repeatx).astype(int)
    jj = np.remainder(j + 1, repeaty).astype(int)
    kk = np.remainder(k + 1, repeatz).astype(int)

    i = np.bitwise_and(i, 255) + base
    j = np.bitwise_and(j, 255) + base
    k = np.bitwise_and(k, 255) + base
    ii = np.bitwise_and(ii, 255) + base
    jj = np.bitwise_and(jj, 255) + base
    kk = np.bitwise_and(kk, 255) + base

    # internal coordinates
    fx = x - i
    fy = y - j
    fz = z - k
    # fade factors
    u = fade(fx)
    v = fade(fy)
    w = fade(fz)

    A = p[i]
    AA = p[A + j]
    AB = p[A + jj]
    B = p[ii]
    BA = p[B + j]
    BB = p[B + jj]

    # noise components
    n000 = gradient(p[AA + k], fx, fy, fz)
    n001 = gradient(p[BA + k], fx - 1, fy, fz)
    n010 = gradient(p[AB + k], fx, fy - 1, fz)
    n011 = gradient(p[BB + k], fx - 1, fy - 1, fz)
    n100 = gradient(p[AA + kk], fx, fy, fz - 1)
    n101 = gradient(p[BA + kk], fx - 1, fy, fz - 1)
    n110 = gradient(p[AB + kk], fx, fy - 1, fz - 1)
    n111 = gradient(p[BB + kk], fx - 1, fy - 1, fz - 1)

    # combine noises
    x1 = lerp(n000, n001, u)
    x2 = lerp(n010, n011, u)
    x3 = lerp(n100, n101, u)
    x4 = lerp(n110, n111, u)

    x5 = lerp(x1, x2, v)
    x6 = lerp(x3, x4, v)

    return lerp(x5, x6, w)


def lerp(a, b, x):
    "linear interpolation"
    return a + x * (b - a)


def fade(t):
    "6t^5 - 15t^4 + 10t^3"
    return 6 * t ** 5 - 15 * t ** 4 + 10 * t ** 3


def gradient(h, x, y, z):
    "grad converts h to the right gradient vector and return the dot product with (x,y)"
    vectors = np.array([
        [0, 1, 1, 1], [0, 1, 1, -1], [0, 1, -1, 1], [0, 1, -1, -1],
        [0, -1, 1, 1], [0, -1, 1, -1], [0, -1, -1, 1], [0, -1, -1, -1],
        [1, 0, 1, 1], [1, 0, 1, -1], [1, 0, -1, 1], [1, 0, -1, -1],
        [-1, 0, 1, 1], [-1, 0, 1, -1], [-1, 0, -1, 1], [-1, 0, -1, -1],
        [1, 1, 0, 1], [1, 1, 0, -1], [1, -1, 0, 1], [1, -1, 0, -1],
        [-1, 1, 0, 1], [-1, 1, 0, -1], [-1, -1, 0, 1], [-1, -1, 0, -1],
        [1, 1, 1, 0], [1, 1, -1, 0], [1, -1, 1, 0], [1, -1, -1, 0],
        [-1, 1, 1, 0], [-1, 1, -1, 0], [-1, -1, 1, 0], [-1, -1, -1, 0]])

    g = vectors[h % 15]

    return g[..., 0] * x + g[..., 1] * y + g[..., 2] * z
