# Perlin Noise with Particles in Python

## Resulting image
![Alt text](images/1.png?raw=true "First image")
![Alt text](images/2.png?raw=true "Second image")

Idea came from: https://pierpaololucarelli.com/2018/02/09/java-perlin-noise-visualisation/
